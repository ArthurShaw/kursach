source "https://rubygems.org"

ruby "2.3.0"

gem "rails", "4.2.3"
gem "pg"

# assets
gem "autoprefixer-rails"
gem "jquery-rails"
gem "jquery-ui-rails"
gem "sass-rails", "~> 5.0.0"
gem "therubyracer", platforms: :ruby
gem "uglifier", ">= 1.3.0"
gem 'paperclip'
gem "paranoia", "~> 2.2"

# views
gem "simple_form"
gem "font-awesome-rails"

# all other gems
gem "devise"
gem "devise_invitable"
gem "kaminari"
gem "responders"
gem "slim"
gem "thin"
gem "puma"
gem "turbolinks"
gem 'activeadmin', github: 'activeadmin'
gem 'activeadmin-ajax_filter'
gem 'activeadmin_addons'
gem 'ckeditor', github: 'galetahub/ckeditor'
gem "acts_as_list"
gem "roo"
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'

group :test do
  gem "simplecov", require: false
  gem "database_cleaner"
  gem "email_spec"
  gem "formulaic"
  gem "launchy"
  gem "shoulda-matchers", require: false
  gem "timecop"
  gem "webmock", require: false
end

group :development, :test do
  gem "awesome_print"
  gem "brakeman", "~>3.0.5", require: false
  gem "bundler-audit"
  gem "byebug"
  gem "dotenv-rails"
  gem "factory_girl_rails"
  gem "fuubar", "~> 2.0.0"
  gem "jasmine", "> 2.0"
  gem "jasmine-jquery-rails"
  gem "pry-rails"
  gem "rails_best_practices", "~> 1.15.7"
  gem "rspec-rails", "~> 3.0"
  gem "rubocop"
  gem "scss_lint", require: false
end

group :development do
  gem "bullet"
  gem "foreman"
  gem "letter_opener"
  gem "quiet_assets"
  gem "spring"
  gem "spring-commands-rspec"
  gem "web-console", "~> 2.0"
  gem "guard-rspec", require: false
  gem 'capistrano',         require: false
  gem 'capistrano-rbenv',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
end
