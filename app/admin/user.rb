ActiveAdmin.register User do
  include ActiveAdmin::AjaxFilter

  config.clear_action_items!
  menu label: 'Пользователи'
  actions :all, except: [:edit, :create]

  filter :first_name, label: 'Имя'
  filter :last_name, label: 'Фамилия'
  filter :organisation, label: 'Организация'
  filter :phone, label: 'Телефон'
  filter :email

  index do
    selectable_column
    id_column

    column "Имя" do |user|
      user.first_name
    end

    column 'Фамилия' do |user|
      user.last_name
    end

    column 'Отчество' do |user|
      user.father_name
    end

    column "Организация" do |user|
      user.organisation
    end

    column "Телефон" do |user|
      user.phone
    end
    column :email
    column :created_at, label: 'Дата регистрации'
    actions
  end

  show do
    attributes_table do
      row :id
      row 'ФИО' do
        "#{user.last_name} #{user.first_name} #{user.father_name}"
      end
      row :organisation
      row :email
      row :phone
      row "Доступные Курсы" do
        user.episodes.map(&:title).join(', ') {|e| e.title }
      end
      row :created_at, label: 'Дата регистрации'
    end
    active_admin_comments
  end

  form do |f|
    f.inputs 'Доступные эпизоды' do
      f.input :first_name, label: 'Имя'
      f.input :last_name, label: 'Фамилия'
      f.input :father_name, label: 'Отчество'
    end
    f.actions
  end
  
  csv do 
    column :email
  end
end
