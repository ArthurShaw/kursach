ActiveAdmin.register Episode do
  permit_params :title, :content, :start_at

  menu label: 'Курсы'

  index do
    selectable_column
    column :id
    column :title
    column :position
    column :created_at
    actions
  end

  form do |f|
    f.inputs "Детали курса" do
      f.input :title
      f.input :content, as: :ckeditor
    end
    f.actions
  end
end
