ActiveAdmin.register Access do
  include ActiveAdmin::AjaxFilter
  
  permit_params :user_id, :episode_id, :until_at
  
  menu label: 'Доступы'
  
  filter :user, label: 'Пользователь', as: :ajax_select,
          data: {
            search_fields: [:last_name],
            static_ransack: { active_eq: true },
            limit: 100000
          }
  filter :episode, label: 'Курс', as: :ajax_select,
         data: {
           search_fields: [:title],
           static_ransack: { active_eq: true },
           limit: 100000
         }
  filter :user_organisation, label: 'Организация', as: :string, collection: User.organisations
  
  index do
    selectable_column
    column :id
    column 'Пользователь' do |a|
      "#{a.user.try(:first_name)} #{a.user.try(:last_name)} #{a.user.try(:father_name)}"
    end
    column 'Курс' do |c|
      "#{c.episode.try(:title)}"
    end
    column 'Email' do |a|
      a.user.email
    end
    column 'Дата регистрации' do |a|
      l(a.user.created_at)
    end
    column 'Доступен до', :until_at
    actions
  end
  
  form do |f|
    f.inputs "Детали доступа" do
      f.input :user, label: 'Пользователь', member_label: proc{|u| u.name_for_select },as: :ajax_select,
              data: {
                  url: filter_admin_users_path,
                  display_fields: [:first_name, :email],
                  search_fields: [:last_name, :email],
                  static_ransack: { active_eq: true },
                  limit: 100000
              }
      f.input :episode, label: 'Курс', as: :ajax_select,
              data: {
                search_fields: [:title],
                static_ransack: { active_eq: true },
                limit: 100000
              }
      f.input :until_at, as: :datepicker, label: 'Доступен до'
    end
    f.actions
  end
end
