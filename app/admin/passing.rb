ActiveAdmin.register Passing do
  include ActiveAdmin::AjaxFilter

  config.clear_action_items!
  menu label: 'Результаты'
  actions :all, except: [:edit, :create]

  filter :user, label: 'Пользователь', as: :ajax_select,
         data: {
           search_fields: [:last_name],
           static_ransack: { active_eq: true },
           limit: 100000
         }
  filter :task, label: 'Модуль', as: :ajax_select,
         data: {
           search_fields: [:title],
           static_ransack: { active_eq: true },
           limit: 100000
         }

  filter :episode, label: 'Курс', as: :ajax_select,
         data: {
             search_fields: [:title],
             static_ransack: { active_eq: true },
             limit: 100000
         }

  filter :user_organisation, label: 'Организация', as: :string, collection: User.organisations
  filter :created_at, label: 'Дата сдачи'

  controller do
    def scoped_collection
      if params[:q] && params[:q][:task_episode_id_eq]
        Passing.joins(:task).where('main = true')
      else
        Passing.joins('INNER JOIN tasks ON tasks.id = passings.task_id').where('tasks.main IS TRUE')
      end
    end
  end

  show do
    attributes_table do
      row :id
      row 'Модуль' do
        if passing.task.deleted?
          "#{passing.task.title} (DELETED)"
        else
          passing.task.title
        end
      end
      row 'Пользователь' do
        "#{passing.user.last_name} #{passing.user.first_name}"
      end
      row 'Удалить результат' do
        link_to 'Удалить', delete_passing_admin_passing_path , notice: 'Результат удален'
      end
    end
    active_admin_comments
  end

  member_action :delete_passing, method: :get do
    task = resource.task
    user = resource.user
    if task&.attempts
      attempt = task.attempts.where(user_id: user.id)
      attempt.destroy_all
    end
    resource.destroy
    redirect_to admin_passings_path, notice: "Удалено"
  end

  index title: 'Результаты' do
    column 'Id' do |a|
      a.id
    end

    column 'Пользователь' do |a|
      "#{a.user.first_name} #{a.user.last_name} #{a.user.father_name}"
    end

    column 'Организация' do |a|
      a.user.organisation
    end

    column 'Курс' do |a|
      a.task&.episode.title
    end

    column 'Модуль' do |a|
      if a.task.deleted?
        "#{a.task.title} (DELETED)"
      else
        a.task.title
      end
    end

    column 'Результат' do |a|
      a.points
    end

    column 'Ответы' do |a|
      link_to 'Посмотреть', results_passing_path(a)
    end

    column 'Дата сдачи' do |a|
      a.created_at.strftime('%d/%m/%Y %H:%M')
    end
    actions
  end

end
