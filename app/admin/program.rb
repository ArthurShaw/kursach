ActiveAdmin.register Program do
  permit_params :excuse_message, :start_time

  form do |f|
    f.inputs 'Текст до начала игры' do
      f.input :excuse_message, as: :ckeditor
      f.input :start_time, as: :string, input_html: { id: "datetimepicker3" }
    end
    f.actions
  end
end
