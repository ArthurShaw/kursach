ActiveAdmin.register Task do
  menu label: 'Модули'

  permit_params :title, :content, :points, :episode_id, :questions_number, :position, :main,
  timer_attributes: [:value_in_minutes],
  questions_attributes: [ :id,
    :text, :question_type, :_destroy,
    answer_options_attributes: [:id, :text, :right, :_destroy]
  ]

  filter :episode, label: 'Курс', as: :select, collection: Episode.all
  filter :title, label: 'Название'
  index do
    selectable_column
    column :id
    column 'Курс' do |e|
      task = Task.find_by(id: e.id)
      task&.episode&.title
    end
    column :position
    column :title
    actions
  end

  show do
    attributes_table do
      row :id
      row "Курс" do
        task.episode&.title
      end
      row :position
      row :title
      row 'Удалить вопросы' do
        link_to 'Удалить', delete_test_admin_task_path , notice: 'Вопросы данного модуля удалены'
      end
    end
    active_admin_comments
  end

  member_action :delete_test, method: :get do
    resource.questions.destroy_all
    redirect_to :back, notice: "Удалено"
  end

  form do |f|
    f.inputs "Детали модуля" do
      f.input :episode
      f.input :position
      f.input :main, label: 'Главный?'
      f.input :title
      f.input :content, as: :ckeditor
      f.input :questions_number, label: 'Кол-во отображаемых вопросов'
      f.inputs "Таймер" do
        f.semantic_fields_for :timer, allow_destroy: true, new_record: true do |timer|
          timer.input :value_in_minutes
        end
      end

      f.inputs "Вопросы" do
        f.has_many :questions do |question|
          question.input :text, as: :string
          question.input :_destroy, :as => :boolean, required: false, label: 'Remove'

          question.has_many :answer_options do |answer_option|
            answer_option.input :text, as: :string
            answer_option.input :right
            answer_option.input :_destroy, :as => :boolean, :required => false, :label => 'Remove'
          end
        end
      end
    end
    f.actions
  end
end
