$(function() {
  var attemptTime = $("#attempt_time").val();
  var currentTime = $("#current_time").val();
  var timerCountInMilliseconds = $("#timer_value_in_seconds").val() * 1000;

  var currentTimeParsed = new Date(currentTime);
  var attemptTimeParsed = new Date(attemptTime);

  if(attemptTimeParsed < currentTimeParsed){
    $("#time-left").removeClass("hidden");
    $("#time-is-out").addClass("hidden");
  } else {
    $("#time-is-out").removeClass("hidden");
    $("#time-left").addClass("hidden");

    return false;
  }

  setInterval(function(){
    currentTimeParsed.setSeconds(currentTimeParsed.getSeconds() + 1);

    var startedAt = currentTimeParsed - attemptTimeParsed;
    var timeLeft = timerCountInMilliseconds - startedAt;

    if(timeLeft <= 0){
      $("#time-is-out").removeClass("hidden");
      $("#time-left").addClass("hidden");
      $("#test-submit").click();

      return false;
    }

    var startsInHours = Math.floor(timeLeft / (1000 * 3600));
    var startsInMinutes = Math.floor(timeLeft / (1000 * 60)) - startsInHours * 60;
    var startsInSeconds = Math.floor(timeLeft / 1000) - startsInMinutes * 60 - startsInHours * 3600;

    if(startsInHours > 0)
      $(".show-time-in-hours").html(startsInHours + " часов ");
    if(startsInMinutes > 0)
      $(".show-time-in-minutes").html(startsInMinutes + " минут ");
    if(startsInSeconds > 0)
      $(".show-time-in-seconds").html(startsInSeconds + " секунд ");
  },1000);
});
