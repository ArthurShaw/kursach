
$(document).ready(function(){

	//Code to center the subscription pup-up box
	$box = $('#box');
	$box.css({
		'left' : '50%',
		'top' : '50%',
		'margin-left' : -$box.width()/2 + 'px',
		'margin-top' : -$box.height()/2 + 'px'
	});


	//Subscription pup-up
	$('.start_btn').click(function(){
		$('#lightbox').width($(window).width()).height($(window).height()).fadeIn(200);
		$('#box').fadeIn(200);

		return false;
	});

	$('#lightbox, .close').click(function(){
		$('#lightbox').width(0).height(0).fadeOut(200);
		$('#box').fadeOut(200);

		return false;
	});


});


$(document).ready(function(){

	$dbox = $('#dbox');
	$dbox.css({
		'left' : '50%',
		'top' : '50%',
		'margin-left' : -$dbox.width()/2 + 'px',
		'margin-top' : -$dbox.height()/2 + 'px'
	});


	$('.work_title').click(function(){
		$('#descriptbox').width($(window).width()).height($(window).height()).fadeIn(200);
		$('#dbox').fadeIn(200);

		return false;
	});

	$('#descriptbox, .close').click(function(){
		$('#descriptbox').width(0).height(0).fadeOut(200);
		$('#dbox').fadeOut(200);

		return false;
	});


});
