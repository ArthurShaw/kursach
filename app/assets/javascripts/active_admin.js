//= require jquery
//= require jquery.datetimepicker.full
//= require active_admin/base
//= require selectize
//= require activeadmin-ajax_filter


$(function() {

  jQuery.datetimepicker.setLocale("ru");
  $("#datetimepicker3").datetimepicker({
    format: "Y-m-d H:i",
    showTimezone: true
  });
});
