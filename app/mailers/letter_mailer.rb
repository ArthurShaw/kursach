class LetterMailer < ApplicationMailer
  layout 'letter_mailer'

  def order(letter)
    @letter = letter
    @url = letters_url
    mail(to: 'maot.kazan@mail.ru', subject: 'Заявка с Системы ДО')
  end
end