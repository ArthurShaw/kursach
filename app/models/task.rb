class Task < ActiveRecord::Base
  acts_as_paranoid

  has_many :questions,  -> { order 'created_at asc' }, dependent: :destroy
  has_one :timer, dependent: :destroy

  has_many :passings
  has_many :attempts, dependent: :destroy

  belongs_to :episode
  validates :episode, :title, :content, presence: true

  accepts_nested_attributes_for :questions, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :timer, allow_destroy: true

#  delegate :text, :question_type, to: :question, allow_nil: true

  after_initialize :set_question
  after_initialize :set_timer
  before_validation :check_timer

  acts_as_list scope: :episode

  scope :completed, -> (user) { includes(:passings).where(passings: { user: user }) }
  scope :current, lambda { |user|
    return completed(user).last.lower_item if completed(user).present?

    first
  }

  def text
    self.questions.to_a
  end

  def question_type
    self.questions.to_a
  end

  def completed?(user)
    passings.where(user: user).size > 0
  end

  def not_expired?(user)
    timer.persisted? && ((DateTime.current.to_i - attempts.where(user: user)[0].created_at.to_i) < timer.value_in_minutes.minutes)
  end

  def current?(user)
    self.episode.current?(user) && self == self.episode.tasks.current(user)
  end

  def check_timer
    timer.delete if timer.value_in_minutes.blank?
  end

  def option_question
    OptionQuestion.where(task: self)[0]
  end

  def set_question
    build_questions unless questions
  end

  def set_timer
    build_timer unless timer
  end
end
