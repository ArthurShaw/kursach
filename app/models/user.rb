class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :first_name, :last_name, :organisation, presence: true
  
  validates :email, uniqueness: true

  has_many :attempts, dependent: :destroy

  has_many :option_answers, dependent: :destroy
  has_many :passings, dependent: :destroy
  
  has_many :accesses, dependent: :destroy
  has_many :episodes, through: :accesses
  
  def points_for_task(task)
    return task.points if task.question.question_type == "TextQuestion"

    task.points / (option_answers.where(question: task.question).size + 1)
  end

  def name_for_select
    "#{full_name} (#{email}, #{created_at.strftime('%d/%m/%y')})"
  end
  
  def full_name
    "#{last_name} #{first_name} #{father_name}"
  end
  
  def self.organisations
    pluck(:organisation).reject { |e| e.blank?}.uniq
  end

  def points
    points = 0

    # Passing.where(user: self).find_each do |pass|
    #   points += pass.points
    # end

    points
  end

  def progress(episode)
    (episode.tasks.completed(self).size.to_f / episode.tasks.size).round(2) * 100
  end

  def accepted_rule?
    rules_acception
  end

end
