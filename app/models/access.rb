class Access < ActiveRecord::Base
  belongs_to :user
  belongs_to :episode
  
  validates :user_id, :episode_id, :until_at, presence: true
end
