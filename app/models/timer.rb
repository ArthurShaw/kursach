class Timer < ActiveRecord::Base
  validates :value_in_minutes, numericality: { only_integer: true, greater_than: 0 }
  belongs_to :task
end
