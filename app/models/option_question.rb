class OptionQuestion < Question
  def right_answer?(answer_id)
    answer = answer_options.find(answer_id)
    answer.right
  end
end
