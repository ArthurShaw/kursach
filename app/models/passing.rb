class Passing < ActiveRecord::Base
  belongs_to :user
  belongs_to :task, ->{ unscope(where: :deleted_at) }
  has_one :episode, through: :task

  serialize :answers
end
