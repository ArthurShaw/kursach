class Question < ActiveRecord::Base
  belongs_to :task
  belongs_to :passing
  has_many :answer_options, -> { order(position: :asc) }, dependent: :destroy
  has_many :option_answers, dependent: :destroy

  validates :text, presence: true, allow_blank: false

  accepts_nested_attributes_for :answer_options,
                                allow_destroy: true

  attr_accessor :answer_option_id

  before_create :set_type
  def set_type
    self.question_type = 'OptionQuestion'
  end
  
  def right_answers
    answer_options.where(right: true).count
  end
end
