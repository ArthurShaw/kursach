class Episode < ActiveRecord::Base
  has_many :tasks, -> { order(position: :asc) }, dependent: :destroy
  has_many :passings, through: :tasks

  has_many :accesses, dependent: :destroy
  has_many :users, through: :accesses

  scope :inactive, -> { where("start_at > ?", DateTime.current) }
  scope :active, -> { where("start_at < ? OR start_at is NULL", DateTime.current) }

  acts_as_list

  scope :current, lambda { |user|
    return completed(user).last.lower_item if completed(user).present?

    first
  }

  def completed?(user)
    tasks.size > 0 && tasks.completed(user).size == tasks.size
  end

  def current?(user)
    higher_item && higher_item.completed?(user) || first?
  end
end
