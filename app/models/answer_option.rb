class AnswerOption < ActiveRecord::Base
  validates :text, presence: true, allow_blank: false
  has_many :option_answers, dependent: :destroy

  belongs_to :question
end
