class PagesController < ApplicationController

  def success
  end

  def caution
    @task = Task.find(params[:task_id])
  end

  def rules
    @letter = Letter.new
  end

  def excuse
    @excuse_page = Program.first
  end

  def wait
    @episode = Episode.find(params[:episode_id])
  end

  def contacts

  end
end
