class PassingsController < ApplicationController
  before_action :authenticate_user!

  def create
    hash = Hash.new []
    passing = Passing.new(passing_params)
    task = Task.with_deleted.find(passing.task_id)
    selected_questions = session[:qids].map { |id| task.questions.find(id)}
    pan = passing.answers.reject { |e| e.blank?}
    user_answers = selected_questions.map { |q| q.answer_options.where(id: pan)}.flatten
    user_answers.each { |ua| hash[ua.question.id] += [ua]}
    right_hash = Hash.new
    hash.each do |key, value|
      if Question.find(key).right_answers == value.count
        right_hash[key] = value.all? { |ao| ao.right? == true}
      else
        right_hash[key] = false
      end
    end
    rights = right_hash.count { |key, value| value == true}
    points = rights.to_f / session[:questions].count.to_f * 100
    passing.points = points
    if passing.save
      respond_to do |format|
        format.html { redirect_to results_passing_path(passing) }
        format.js { redirect_to results_passing_path(passing) }
      end

    end


  end

  def results
    hash = Hash.new []
    @passing = Passing.find(params[:id])
    @task = Task.with_deleted.find(@passing.task_id)
    pan = @passing.answers.reject { |e| e.blank?}
    @user_answers = @task.questions.map { |q| q.answer_options.where(id: pan)}.flatten
    @user_answers.each { |ua| hash[ua.question.id] += [ua]}
    right_hash = Hash.new
    hash.each do |key, value|
      if Question.find(key).right_answers == value.count
        right_hash[key] = value.all? { |ao| ao.right? == true}
      else
        right_hash[key] = false
      end
    end
    rights = right_hash.count { |key, value| value == true}
    @right_answers = rights
    @user_questions = right_hash
  end

  private

  def passing_params
    params.require(:passing).permit(:user_id, :task_id, :points, answers: [])
  end

  def questions_list
    task = Task.with_deleted.find(params[:id])
    qids = session[:questions].map { |q| q.id}
    selected_questions = qids.map { |id| task.questions.find(id)}
  end
end