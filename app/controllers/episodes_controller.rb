class EpisodesController < ApplicationController
  def index
    @active_episodes = current_user.episodes.where('until_at > ?', DateTime.now)
  end

  def show
    @episode = Episode.find(params[:id])
    @tasks = @episode.tasks.all
  end
end
