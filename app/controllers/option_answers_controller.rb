class OptionAnswersController < ApplicationController


  private

  def answer_params
    params.require(:question).permit(:answer_option_id) if params[:question]
  end

  def question
    Question.find(params[:question_id])
  end

  def answer_option
    AnswerOption.find(answer_params[:answer_option_id]) if answer_params
  end

  def create_answer
    OptionAnswer.create(
      question: question,
      user: current_user,
      answer_option: answer_option
    )
  end

  def task
    question.task
  end

  def wrong_answers
    @wrong_answers ||= OptionAnswer.includes(:answer_option).where(user: current_user, question: question, answer_options: {right: false}).size + 1

    @wrong_answers
  end

  def create_passing
    Passing.create(passing_params)
  end


end
