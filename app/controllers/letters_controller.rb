class LettersController < ApplicationController

  def create
    letter = Letter.new(letter_params)
    if letter.save
      LetterMailer.order(letter).deliver_now
      redirect_to root_path
    else
      redirect_to new_user_registration_path
    end
  end

  private

  def letter_params
    params.require(:letter).permit(:name, :phone, :course)
  end
end
