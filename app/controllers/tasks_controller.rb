class TasksController < ApplicationController
  def current
    excuse_page = Program.first
    current_episode = Episode.first
    Episode.active.find_each do |episode|
      break unless episode.completed?(current_user)
      current_episode = episode.lower_item
    end

    if current_episode.present? && current_episode.start_at && current_episode.start_at > DateTime.current
      return redirect_to wait_episode_pages_url(current_episode)
    end

    return redirect_to success_pages_url if current_episode.blank?

    if excuse_page.present? && excuse_page.start_time > DateTime.current
      return redirect_to excuse_pages_url
    end

    @task = current_episode.tasks.current(current_user)
    @passing = Passing.new
  end

  def show
    @task = Task.find(params[:id])
    if @task.questions.present?
      session[:questions] = @task.questions.sample(@task.questions_number)
      session[:qids] = session[:questions].map { |q| q.id}
      @attempt = Attempt.find_or_create_by(user: current_user, task: @task)
    end
  end
end
