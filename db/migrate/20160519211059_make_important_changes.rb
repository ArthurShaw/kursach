class MakeImportantChanges < ActiveRecord::Migration
  def change
    remove_column :option_answers, :text

    rename_column :tasks, :nuts, :points
    rename_column :episodes, :isActive, :is_active
  end
end
