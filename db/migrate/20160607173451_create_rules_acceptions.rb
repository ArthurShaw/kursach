class CreateRulesAcceptions < ActiveRecord::Migration
  def change
    create_table :rules_acceptions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :rules_page, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
