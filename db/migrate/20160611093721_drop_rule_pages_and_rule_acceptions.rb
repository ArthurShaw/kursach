class DropRulePagesAndRuleAcceptions < ActiveRecord::Migration
  def change
    drop_table :rules_acceptions
    drop_table :rules_pages
  end
end
