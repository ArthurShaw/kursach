class CreateAttachmentAnswers < ActiveRecord::Migration
  def change
    create_table :attachment_answers do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :question, index: true, null: false, foreign_key: true
      t.string :attachment, null: false

      t.timestamps null: false
    end
  end
end
