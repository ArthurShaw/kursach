class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :text
      t.references :task, index: true, foreign_key: true
      t.string :type

      t.timestamps null: false
    end
  end
end
