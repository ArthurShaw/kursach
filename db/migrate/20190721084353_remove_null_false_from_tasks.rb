class RemoveNullFalseFromTasks < ActiveRecord::Migration
  def change
    change_column_null :passings, :task_id, true
  end
end
