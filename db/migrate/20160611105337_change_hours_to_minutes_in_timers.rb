class ChangeHoursToMinutesInTimers < ActiveRecord::Migration
  def change
    rename_column :timers, :value_in_hours, :value_in_minutes
  end
end
