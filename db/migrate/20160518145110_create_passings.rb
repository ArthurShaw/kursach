class CreatePassings < ActiveRecord::Migration
  def change
    create_table :passings do |t|
      t.integer :points
      t.string :user_id
      t.integer :task_id

      t.timestamps null: false
    end
  end
end
