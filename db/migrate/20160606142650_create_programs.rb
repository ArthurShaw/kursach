class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.text :excuse_message
      t.datetime :start_time
    end
  end
end
