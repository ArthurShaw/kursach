class ChangeEpisodeIdForTasks < ActiveRecord::Migration
  def change
    remove_foreign_key :tasks, :episodes
  end
end
