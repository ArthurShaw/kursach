class AddTimer < ActiveRecord::Migration
  def change
    create_table :timers do |t|
      t.integer :value_in_hours
      t.references :task, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
