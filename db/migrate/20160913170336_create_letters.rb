class CreateLetters < ActiveRecord::Migration
  def change
    create_table :letters do |t|
      t.string :name
      t.string :phone
      t.string :course
      t.timestamps null: false
    end
  end
end
