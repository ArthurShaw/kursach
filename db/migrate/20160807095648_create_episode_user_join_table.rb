class CreateEpisodeUserJoinTable < ActiveRecord::Migration
  def change
    create_join_table :episodes, :users do |t|
      t.index :episode_id
      t.index :user_id
    end
  end
end
