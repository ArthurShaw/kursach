class AddTeamToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :team, :string
  end
end
