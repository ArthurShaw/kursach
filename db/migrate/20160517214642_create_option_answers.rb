class CreateOptionAnswers < ActiveRecord::Migration
  def change
    create_table :option_answers do |t|
      t.text :text
      t.references :question, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :answer_option, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
