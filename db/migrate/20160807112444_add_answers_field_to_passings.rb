class AddAnswersFieldToPassings < ActiveRecord::Migration
  def change
    add_column :passings, :answers, :text, null: false, default: ""
  end
end
