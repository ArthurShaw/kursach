class RemoveStepFromTask < ActiveRecord::Migration
  def change
    remove_column :tasks, :step, :integer
  end
end
