class ChangePassingsTable < ActiveRecord::Migration
  def change
    change_column :passings, :user_id, "integer USING CAST(user_id AS integer)", null: false, index: true, foreign_key: true
    change_column :passings, :task_id, :integer, null: false, index: true, foreign_key: true
  end
end
