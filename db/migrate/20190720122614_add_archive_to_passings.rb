class AddArchiveToPassings < ActiveRecord::Migration
  def change
    add_column :passings, :archive, :boolean, default: false
  end
end
