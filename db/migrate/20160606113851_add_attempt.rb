class AddAttempt < ActiveRecord::Migration
  def change
    create_table :attempts do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :task, index: true, null: false, foreign_key: true

      t.timestamps null: false
    end
  end
end
