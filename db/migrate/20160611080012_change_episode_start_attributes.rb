class ChangeEpisodeStartAttributes < ActiveRecord::Migration
  def change
    remove_column :episodes, :is_active, :boolean
    add_column :episodes, :start_at, :datetime
  end
end
