class CreateAnswerOptions < ActiveRecord::Migration
  def change
    create_table :answer_options do |t|
      t.text :text
      t.integer :position
      t.boolean :right, default: false
      t.references :question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
