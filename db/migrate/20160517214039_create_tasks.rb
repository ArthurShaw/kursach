class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :episode, index: true, foreign_key: true
      t.string :type
      t.string :title
      t.text :content
      t.integer :nuts

      t.timestamps null: false
    end
  end
end
