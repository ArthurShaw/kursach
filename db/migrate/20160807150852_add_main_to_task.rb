class AddMainToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :main, :boolean
  end
end
