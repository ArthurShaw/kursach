class ChangeDefaultToTasks < ActiveRecord::Migration
  def change
    change_column_default :tasks, :questions_number, 15
  end
end
