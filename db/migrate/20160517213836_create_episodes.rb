class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :title
      t.text :content
      t.boolean :isActive

      t.timestamps null: false
    end
  end
end
