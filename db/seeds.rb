# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create!(
  email: 'admin@example.com',
  password: 'Password1',
  password_confirmation: 'Password1'
) if AdminUser.where(email: "admin@example.com").empty?


User.create!(
  email: 'user@example.com',
  password: 'Password1',
  password_confirmation: 'Password1'
) if User.where(email: "user@example.com").empty?


user = User.find_by(email: "user@example.com")
Profile.find_or_create_by(user: user, name: "abc")

Episode.find_or_create_by(content: "<b>Hello</b>", title: "Новый эпизод")
Episode.find_or_create_by(content: "<b>Second</b>", title: "Второй эпизод")

3.times do |n|
  task = Task.create!(episode: Episode.find(1), title: "Задача #{n}", content: "<h1>World #{n + 1}</h1>", points: 10, "question_attributes" => {"text" => "q1", "question_type" => "TextQuestion"})

  Passing.find_or_create_by(user: User.last, task: task, points: 5)
  Question.find_or_create_by(text: "Question #{n}", task: task, question_type: "TextQuestion")
end

3.times do |n|
  task = Task.create!(episode: Episode.find(2), title: "Задача 2 #{n}", content: "<h1>World 2 #{n + 1}</h1>", points: 10, "question_attributes" => {"text" => "q1", "question_type" => "TextQuestion"})

  Passing.find_or_create_by(user: User.last, task: task, points: 5)
  question = Question.find_or_create_by(text: "Question #{n}", task: task, question_type: "OptionQuestion")
  AnswerOption.find_or_create_by(question: question, text: "Answer #{n}", right: true)
  3.times do |nn|
    AnswerOption.find_or_create_by(question: question, text: "Answer #{nn}", right: false)
  end
end
