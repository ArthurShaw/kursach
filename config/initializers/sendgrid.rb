ActionMailer::Base.smtp_settings = {
  address:              "smtp.sendgrid.net",
  port:                 "587",
  authentication:       :plain,
  user_name:            ENV["SENDGRID_USERNAME"],
  password:             ENV["SENDGRID_PASSWORD"],
  domain:               "dai5.level90.net",
  enable_starttls_auto: true,
} if ENV["SENDGRID_USERNAME"]
