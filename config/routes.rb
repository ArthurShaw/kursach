Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    registrations: 'users/registrations'
  }

  get 'contacts' => 'pages#contacts'
  get 'caution' => 'pages#caution'
  resources :passings, only: %i(create) do
    get :results, action: :results, on: :member
  end

  resources :letters, only: %i(create)
  resources :questions, only: [] do
    resources :option_answers, only: [] do
      post :answer, action: :answer, on: :collection
    end

    resources :text_answers, only: [] do
      post :answer, action: :answer, on: :collection
    end

    resources :encouraged_answers, only: [] do
      post :answer, action: :answer, on: :collection
    end

    resources :attachment_answers, only: [] do
      post :answer, action: :answer, on: :collection
    end
  end

  resources :episodes, only: [:index, :show] do
    resources :pages, only: [] do
      get :wait, on: :collection
    end
  end

  resources :tasks, only: [:show] do
    get :current, action: :current, on: :collection
  end

  resources :pages, only: [] do
    get :success, action: :success, on: :collection
    get :excuse, action: :excuse, on: :collection
    get :rules, action: :rules, on: :collection
  end



  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  authenticated :user do
    root "episodes#index", as: :authenticated_root
  end

  root to: 'pages#rules'
end
